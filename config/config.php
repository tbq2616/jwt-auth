<?php


return [
    'secret'      => 'JWT_SECRET',
    //Asymmetric key
    'public_key'  => 'JWT_PUBLIC_KEY',
    'private_key' => 'JWT_PRIVATE_KEY',
    'password'    => 'JWT_PASSWORD',
    //JWT time to live
    'ttl'         => 60,
    //Refresh time to live
    'refresh_ttl' => 20160,
    //JWT hashing algorithm
    'algo'        => 'HS256',
    //token获取方式，数组靠前值优先
    'token_mode'    => ['header', 'cookie', 'param'],
    'blacklist_storage' => thans\jwt\provider\storage\Tp5::class,
];
