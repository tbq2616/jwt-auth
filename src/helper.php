<?php

use thans\jwt\command\SecretCommand;
use thans\jwt\provider\JWT as JWTProvider;
use think\Console;
use think\App;
use think\Request;

if (true || strpos(App::VERSION, '6.0') === false) {
    Console::addDefaultCommands([
        SecretCommand::class
    ]);
    // (new JWTProvider(app('request')))->init();
    (new JWTProvider(Request::instance()))->init();
}
